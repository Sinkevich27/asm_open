#include <iostream>
#include <conio.h> 
#include <math.h>
#include <cstdlib>
#include<stdio.h>
#include <cstdio>
#include <locale.h>

//���������  ��������  ������� f, ������� ������� � �������,  �  �����  x, ���������  ���������� � ��� �������. 
//tg(x) (��������� ����� ������ 5 ������ ����)


using namespace std;

double f_accuracy(double x) {
	double f = 0;
	f = tan(x);
	return f;
}

double teylor(double x) {

	double result = 0.0;
	double fact_2 = 2;
	double fact_3 = 6;
	double fact_4 = 16;
	double fact_5 = 120;
	__asm {

		finit													
		fldz
		fld			x
		faddp		st(1), st(0)								//x � st0  
		fldz
		faddp		st(1), st(0)								//x � st0
		fld			x											//x(st0) | x(st1) 
		fmul		st(0), st(0)								//x^2 | x
		fld			st(0)										//x^2 | x^2 | x
		fmul 		st(0), st(0)								//x^4 | x^2 | x			
		fmul 		st(0), st(2)								//x^5 | x^2 | x
		fxch		st(1)										//x^2 | x^5 | x
		fmul		st(0), st(2)								//x^3 | x^5 | x 
		fld			fact_2										//2 | x ^ 3 | x ^ 5 | x
		fld			fact_3										//6 | 2 | x^3 | x^5 | x
		fdivp		st(1), st(0)								//2/6 | x^3 | x^5 | x
		fmulp		st(1), st(0)								//2/6*x^3 |x^5 | x
		fld			fact_4										//16 | 2/6*x^3 | x^5 | x
		fld			fact_5										//120 | 16 | 2/6*x^3 | x^5 | x
		fdivp		st(1), st(0)								//16/120 |2/6*x^3 | x^5 | x
		fmulp		st(2), st(0)								//x^5*16/120 | 2/6*x^3 | x
		fldz													//0 | x^5*16/120 | 2/6*x^3 | x
		faddp		st(3), st(0)								//x | x^5*16/120 | 2/6*x^3
		faddp		st(1), st(0)								//x+2/6*x^3 | x^5*16/120
		faddp		st(1), st(0)								//x+2/6*x^3+x^5*16/120
	}
}

//______________________________________________________________________________________________________________________________________________________________________________

void menu() {
	//Menu
	setlocale(LC_CTYPE, "Russian");
	char clr[] = "cls";
	char enter_x[] = "������� �������� � : \n";
	char result[] = "��������� : %lf \n";
	char accuracy[] = "��������� �������� : %lf \n";
	char tangens[] = "�������� tg(x) : %lf \n\n";
	char anotherPoint[] = "� ��������� ����� ������� ����������, ������� ������ ��������.  \n";
	char str_form[] = "%lf";
	char int_form[] = "%d";
	char start_programm[] = "1. Start programm \n";
	char exit[] = "2. Exit \n";
	int  menu_point;

	//Function
	double x = 0.0;
	double value = 1 / 2.0;
	int	flag = 0;
	int	res = 0.0;
	double	accur = 0.0;


	__asm {

	MENU:

		lea			eax, start_programm
			push		eax
			call		dword ptr printf
			add			esp, 4


			lea			eax, exit
			push		eax
			call		dword ptr printf
			add			esp, 4


			lea			eax, menu_point
			push		eax
			lea			eax, int_form
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			xor eax, eax
			mov			eax, menu_point
			cmp			eax, 1
			je			START_PROGRAMM
			cmp			eax, 2
			je			FIN


			START_PROGRAMM :

		lea			eax, clr
			push		eax
			call		dword ptr system
			add			esp, 4

			lea			eax, enter_x
			push		eax
			call		dword ptr printf
			add			esp, 4


			lea			eax, x
			push		eax
			lea			eax, str_form
			push		eax
			call		dword ptr scanf_s
			add			esp, 8


			fld			x										// ���������, ������������� �� x
			fldz												//0|x
			fxch		st(1)									//x|0
			fcom		st(1)
			fstsw		ax
			sahf
			jc			MAKE_ABS

			fxch		st(1)									//0|X
			fstp		st(0)									//x


			CHECK:
		// ���������, � > pi ?
		fstp		st(0)									//pi|x
			fcom		st(1)
			fstsw		ax
			sahf
			jc			SMALL
			fstp		st(0)									//x
			cmp			flag, 0
			jg			NOT_ABS


			TAN :
		lea			eax, x
			mov			ebx, [eax + 4]
			push		ebx
			mov			ebx, [eax]
			push		ebx
			call		dword ptr teylor
			add			esp, 8
			jmp			PRINT_RES

			PRINT_RES :

		sub			esp, 8
			fstp		qword ptr[esp]
			lea			eax, result
			push		eax
			call		dword ptr printf
			add			esp, 8

			fstp		st(0)									//��������� � ����������� ������� �����
			lea			eax, x
			mov			ebx, [eax + 4]
			push		ebx
			mov			ebx, [eax]
			push		ebx
			call		dword ptr f_accuracy					//��������
			add			esp, 8

			sub			esp, 8
			fstp		qword ptr[esp]
			lea			eax, tangens
			push		eax
			call		dword ptr printf
			add			esp, 16

			finit
			jmp			MENU



			MAKE_ABS :

		mov			flag, 1
			fxch		st(1)									//0|X
			fstp		st(0)									//x
			fabs												//|x|
			fst			x
			jmp			CHECK

			NOT_ABS :

		fchs
			fst			x
			mov			flag, 0
			jmp			TAN


			SMALL :

		fstp		st(0)									//x
			fldpi												//pi|x
			fld			x										//x|pi|x
			fprem												//x/pi ost |pi|x				
			fst			x


			//��������� ��������� x � pi/2, ��� ��������� ������
			fld			x
			fldpi												//pi|x
			fld			value									//1/2|pi|x
			fmul		st(0), st(1)							//pi/2|pi|x
			fcom		st(2)
			fstsw		ax
			sahf
			jz			ERROR_X

			jc			ERR_1

			fstp		st(0)									//pi|x
			fstp		st(0)									//x
			cmp			flag, 0
			jg			NOT_ABS
			jmp			TAN

			ERROR_X :

		lea			eax, anotherPoint
			push		eax
			call		dword ptr printf
			add			esp, 4
			call		dword ptr getchar

			jmp			MENU

			ERR_1 :

		fstp		st(0)									//pi|x
			fsubp		st(1), st(0)
			fst			x
			cmp			flag, 0
			jg			NOT_ABS
			jmp			TAN

			FIN :

	}

}


void main()
{
	menu();
	system("pause");
}


