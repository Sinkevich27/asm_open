#include <stdlib.h>
#include <Windows.h>
#include <stdio.h>
#include <iostream>

char* copyString(char* s, int N) {
	char* res = 0;
	__asm {
		xor esi, esi
		mov ebx, s

		WHILE :
		cmp byte ptr[ebx][esi], 0
			je LEN
			inc esi
			jmp WHILE

			LEN :
		mov eax, N
			mul esi
			inc eax

			push eax
			call dword ptr malloc
			add esp, 4
			mov res, eax

			mov ebx, s
			xor esi, esi
			xor edi, edi
			mov ecx, N
			cmp ecx, 0
			je ZERO

			CYCLE :
	WHILE_INNER:
		mov dl, [ebx][esi]
			cmp dl, 0
			je NEXT_WHILE
			mov[eax][edi], dl
			inc esi
			inc edi
			jmp WHILE_INNER
			NEXT_WHILE :
		xor esi, esi

			loop CYCLE
			ZERO :
		mov byte ptr[eax][edi], 0
	}
}

int main() {
	char inputString[100];
	char* res = 0;
	int N = 0;
	char format[] = "%d";
	copyString(inputString, N);
	__asm {
		//gets_s(s,len)
		mov edx, 100
		push edx
		lea edx, inputString
		push edx
		call dword ptr gets_s;
		add esp, 8

			//scanf_s("%d", &N);
			lea edx, N
			push edx
			lea edx, format
			push edx
			call dword ptr scanf_s
			add esp, 8

			mov edx, N
			push edx
			lea edx, inputString
			push edx
			call dword ptr copyString
			add esp, 8

			//printf(������)
			push eax
			call dword ptr printf
			add esp, 4
	}
	system("pause");
}
