#include <iostream>
#include <conio.h>
#include<stdio.h>
#include<cstdlib>
#include <locale.h>

int Random()
{
	return rand() % 10;
}
int main()
{

	setlocale(0, "rus");
	char choose_size_of_matrix[] = " Matrix size : ";
	char choose_function[] = "1. Create matrix. \n2. Delete matrix. \n3. Read element ij. \n4. Change element ij. \n5  Multiplpy matricies. \n6. Change size of matrix. \n7. Exit. \n ";
	char out_string[] = "%s";
	char choose_menu[] = "%d";
	char clr[] = "cls";
	int menu_point = 0;

	// ������ � �������

	int MATRIX_SIZE = 0;
	int** Matrix = 0;
	int** Matrix2 = 0;
	int** Matrix_Result = 0;
	int** tmp = 0;
	int flag = 0;
	int flag2 = 0;
	int flag3 = 0;
	int flag4 = 0;
	int flagSTR = 4;
	int flagSTR2 = 0;
	int flagSTR3 = 4;
	int x = 0;
	char size_error[] = "Memory exception.\n";
	char empty[] = "Matrix is empty.\n";
	char negative_size[] = "Matrix size must be positive. (> 0)\n";

	// ������

	char* p = "\t%d";
	char* en = "\n";
	char* s = "%s";
	char* empt = "Matrix is emty.\n";

	// �������� �������

	int I = 0;
	int J = 0;
	char choose_element[] = "[i][j] = ";
	char negative_element[] = "Element must be positive. (i,j >0) \n";
	char element_error[] = "Index is greater than matrix size. \n";
	char out_int[] = " ij = %d \n";

	const int PUTELEMENT = 0;

	int flag_M3 = 0;
	//int arr[N];
	//int *p = new int[N];

	__asm {

		
	SIZE_OF_MATRIX:	// ���� �����������

		lea			eax, choose_size_of_matrix
			push		eax
			lea			eax, out_string
			push		eax
			call		dword ptr printf
			add			esp, 8


			lea			eax, MATRIX_SIZE
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			lea			eax, clr											// ������ ������
			push		eax
			call		dword ptr system
			add			esp, 4



			MENU:



		lea         eax, choose_function								// ����� ����
			push        eax
			lea         eax, out_string
			push        eax
			call        dword ptr printf
			add         esp, 8

			lea			eax, menu_point										// ���� ������ ����
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			lea			eax, clr											// ������ ������
			push		eax
			call		dword ptr system
			add			esp, 4

			mov			eax, menu_point										// ����� ����
			cmp			eax, 1
			je			DEC_FLAG
			jl			MENU
			cmp			eax, 2
			je			MATRIX_REMOVAL
			cmp			eax, 3
			je			GET_ELEM
			cmp			eax, 4
			je			PUT_ELEM
			cmp			eax, 5
			je			MEMORY_FOR_M3
			cmp			eax, 6
			je			SIZE_OF_MATRIX
			cmp			eax, 7												// �����
			je			END
			jg			MENU

//Create
			DEC_FLAG :

		mov			eax, 0
			mov			ebx, flag2
			imul		ebx
			mov			flag2, eax
			mov			flag, eax
			jmp			START

			START :

		mov			eax, MATRIX_SIZE						// �������� � ��� ������ �������
			cmp			eax, 0															// ������������� ������
			jl			NEGATIVE_SIZE
			shl			eax, 2; �������� ��� �� 4
			push		eax; �������� ��� � ����
			call		dword ptr malloc					// �������� ������ ��� ������ ����������
			add			esp, 4								// ��������� ����

			cmp			eax, 0
			je			ERROR1
			push		eax									// �������� ���� ��������� � ����
			xor			esi, esi							// esi - �������
			xor			edi, edi

			CYCLE :

		cmp			esi, MATRIX_SIZE						// �������� ������� � ������
			jge			NEXT								// � ������ ��������� ������� ��������� �� ��������� ������

			mov			eax, MATRIX_SIZE					// �������� � ��� ������ ������
			shl			eax, 2								// �������� ��� �� 4
			push		eax
			call		dword ptr malloc					// �������� ��� ���� ������
			add			esp, 4								// ��������� ����

			mov			ebx, [esp]
			mov[ebx][esi * 4], eax
			inc			esi
			jmp			CYCLE



			NEXT :

		pop			eax
			cmp			flag_M3, 1
			je			MULTIPLY
			cmp			flag, 0
			jne			MATRIX2

			MATRIX1 :

		mov			Matrix, eax
			inc			flag
			inc			flag4
			jmp			FILL

			MATRIX2 :

		inc			flag2
			mov			Matrix2, eax
			jmp			FILL

			ERROR1 :

		lea         eax, size_error
			push        eax
			lea         eax, out_string
			push        eax
			call        dword ptr printf
			add         esp, 8
			jmp			MENU

			NEGATIVE_SIZE :

		lea			eax, negative_size
			push		eax
			lea         eax, out_string
			push        eax
			call        dword ptr printf
			add         esp, 8
			jmp			MENU

//Random
		FILL:

		mov		tmp, eax
			xor		esi, esi							// �������� ������� �� �������
			mov		ecx, MATRIX_SIZE					// � ��� ������ �������

			START_FILL1 :

		cmp			esi, ecx							// �������� ������� �� ������� � ������
			jge			FIN_FILL						// � ������ ��������� ������� �� ��������
			xor			edi, edi						// �������� ������� �� ��������

			START_FILL2 :

		cmp			edi, ecx							// �������� ������� �� �������� � ������ �������
			jge			TEMP1							// ���� �� ���������� �������� �� �������

			lea			eax, tmp						// �������� � ���
			mov			eax, [eax]						// ������������ ��������� �� �������
			mov			ebx, [eax][esi * 4]				// ���������� �� ������
			push		ebx								// ��������� �����
			push		ecx								// ��������� ������

			call		dword ptr Random

			pop			ecx								// ������� ������
			pop			ebx								// ������� �����

			mov[ebx][edi * 4], eax						// �������� ��������
			inc			edi								// ��������� �������
			jmp			START_FILL2

			TEMP1 :

		inc			esi
			jmp			START_FILL1

			FIN_FILL :

		
	PRINT:

		xor			esi, esi								// �������� ������� �� �������
			mov			ecx, MATRIX_SIZE					// � ��� ������ �������
			cmp			ecx, 0								// �������� ��������� �� ������� � �����
			je			EMPTY								


			START_PRINT1 :

		cmp			esi, ecx								// �������� ������� �� ������� � ������
			jge			FINISH								// � ������ ��������� ������� �� ��������
			xor			edi, edi							// �������� ������� �� ��������

			START_PRINT2 :

		mov			eax, tmp								// �������� � ���
			cmp			edi, ecx							// �������� ������� �� �������� � ������ �������
			jge			TEMP								// ���� �� ���������� �������� �� �������

			mov			ebx, [eax][esi * 4]					// ��� �� ������
			mov			eax, [ebx][edi * 4]					// ��� �� �������
			push		ebx
			push		ecx
			push		eax
			mov			eax, p
			push		eax
			call		dword ptr printf
			add			esp, 8
			inc			edi
			pop			ecx
			pop			ebx
			jmp			START_PRINT2

			TEMP :

		inc			esi							// ��������� ������� �� �������
			push		ecx
			mov			eax, en
			push		eax
			mov			eax, s
			call		dword ptr printf
			add			esp, 4
			pop			ecx
			jmp			START_PRINT1			// ���� �� ������

			EMPTY :

		lea		    eax, empt
			push		eax
			lea         eax, out_string
			push        eax
			call		dword ptr   printf
			add			esp, 4
			jmp			MENU

			FINISH :

		cmp			flag2, 0
			je			START
			jne			MENU


			MATRIX_REMOVAL :

		cmp			flag4, 0
			je			MENU

			lea			eax, Matrix
			mov			eax, [eax]					// mov eax, Matrix
			push		eax
			mov			ecx, MATRIX_SIZE
			xor			edi, edi
			inc			flag3
			dec			flag4

			FIRST_DELETING :



		mov			edi, ecx
			mov			eax, [esp]
			mov			eax, [eax + ecx * 4 - 4]
			push		eax
			call		dword ptr free
			add			esp, 4
			mov			ecx, edi
			loop		FIRST_DELETING
			call		dword ptr free
			add			esp, 4

			cmp			flag3, 1
			jne			MENU

			//SECOND_DELETING :
			inc			flag3
			lea			eax, Matrix2
			mov			eax, [eax]
			push		eax
			mov			ecx, MATRIX_SIZE
			xor			edi, edi
			jmp			FIRST_DELETING


			

		GET_ELEM:

		lea			eax, choose_element
			push		eax
			lea			eax, out_string
			push		eax
			call		dword ptr printf
			add			esp, 8

			lea         eax, J
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			lea			eax, I
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8


			cmp			flag4, 0
			//je			MENU
			je			EMPTY

			mov			esi, I						// ������� ������� � ��������
			cmp			esi, 0
			jl			SIZE_ERROR1
			cmp			esi, MATRIX_SIZE
			jge			SIZE_ERROR2
			mov			ecx, MATRIX_SIZE
			cmp			esi, ecx
			jg			SIZE_ERROR2
			mov			edi, J
			cmp			edi, 0
			jl			SIZE_ERROR1
			cmp			edi, MATRIX_SIZE
			jge			SIZE_ERROR2
			cmp			edi, ecx
			jg			SIZE_ERROR2

			lea 		eax, Matrix
			mov			eax, [eax]
			mov			ebx, dword ptr[eax][esi * 4]
			mov			eax, dword ptr[ebx][edi * 4]


			push		eax
			lea			eax, out_int
			push		eax
			call		dword ptr printf
			add			esp, 8
			jmp			MENU

			SIZE_ERROR1 :

		lea			eax, negative_element
			push		eax
			lea         eax, out_string
			push        eax
			call		dword ptr   printf
			add			esp, 4
			jmp			MENU

			SIZE_ERROR2 :

		lea			eax, element_error
			push		eax
			lea         eax, out_string
			push        eax
			call		dword ptr   printf
			add			esp, 4
			jmp			MENU

			jmp			MENU


			PUT_ELEM :

		lea			eax, choose_element
			push		eax
			lea			eax, out_string
			push		eax
			call		dword ptr printf
			add			esp, 8

			lea         eax, J
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			lea			eax, I
			push		eax
			lea			eax, choose_menu
			push		eax
			call		dword ptr scanf_s
			add			esp, 8

			cmp			flag4, 0
			je			MENU

			mov			esi, I						// ������� ������� � ��������
			cmp			esi, 0
			jl			ERROR3
			cmp			esi, MATRIX_SIZE
			jge			ERROR4
			mov			ecx, MATRIX_SIZE
			cmp			esi, ecx
			jg			ERROR4
			mov			edi, J
			cmp			edi, 0
			jl			ERROR3
			cmp			edi, MATRIX_SIZE
			jge			ERROR4
			//cmp			edi, ecx
			//jg			ERROR4

			lea			eax, Matrix
			mov			eax, [eax]
			mov			ebx, dword ptr[eax][esi * 4]
			mov			eax, PUTELEMENT
			mov			dword ptr[ebx][edi * 4], eax
			mov			eax, Matrix
			mov			tmp, eax
			jmp			PRINT

			ERROR3 :

		lea			eax, negative_element
			push		eax
			lea         eax, out_string
			push        eax
			call		dword ptr   printf
			add			esp, 4
			jmp			MENU

			ERROR4 :

		lea			eax, element_error
			push		eax
			lea         eax, out_string
			push        eax
			call		dword ptr   printf
			add			esp, 4
			jmp			MENU

			//__________________________________________________________________________________________________________________________________
			//���������

			MEMORY_FOR_M3 :

		inc			flag_M3									// ������� 3 ��� �� �������
			cmp			flag_M3, 1							//���� �� �������
			je			START								//������


			MULTIPLY :

		inc			flag_M3									// 3 ������� �������
			inc			flag_M3
			mov			Matrix_Result, eax

			lea			EDI, Matrix
			mov			edi, [edi]
			mov			edi, [edi]
			lea			ESI, Matrix2
			mov			esi, [esi]
			mov			esi, [esi]
			lea			EBX, Matrix_Result
			mov			ebx, [ebx]
			mov			ebx, [ebx]

			// ���������� ���������� ������������ ������
			mov			edx, MATRIX_SIZE

			MET1 :

		mov			ecx, MATRIX_SIZE
			push		edx

			// ��������� ������ ������� - ����������
			MET2 :

		mov			eax, 0
			mov[ebx], eax								// ��������� �������� �������� ������� - ����������
			push		ecx
			mov			ecx, MATRIX_SIZE


			// ��������� �������� ������ ������� - ����������
			MET3 :

		mov			eax, [edi]								// ������� 1-� ������� � eax
			mov			edx, [esi]							// ��� ������
			imul		edx									//��������� ���������
			add[ebx], eax									// ������ ���������������� ����������
			add			edi, 4								// ������� � ���������� �������� ������ ������ �������
			mov			esi, Matrix2						// �������� ������ ��� 2-� �������
			add			esi, flagSTR
			lea			esi, [esi]
			mov			esi, [esi]
			cmp x, 0										// ������� � ���, ��� �� ������ �������
			je MET4
			add			esi, flagSTR2						//�������� �� ������, ���� �� ������ �������

															//������� � ���������� �������� ������� ������ �������

			MET4 :

		add			flagSTR, 4								// ��� �������� ������ �� ���� ��������
			loop		MET3

			add			ebx, 4								// ������� � ���������� �������� ������ ������� - ����������
			mov			flagSTR, 4							// ��������� �����
			mov			ecx, MATRIX_SIZE
			mov			eax, 4
			imul		ecx									// ���������� ������ � ������
			sub			edi, eax							// ����������� � ������ ������ ������ �������

			add			flagSTR2, 4							// ���������� 4 ����� ��� �������� � ���� �������
			mov			esi, Matrix2

			lea			esi, [esi]							// � ���� �������
			mov			esi, [esi]
			add			esi, flagSTR2
			inc			x
			pop			ecx
			loop		MET2

			mov			EDI, Matrix							// ������� � ���� ������ 1-� �������
			add			edi, flagSTR3
			lea			edi, [edi]
			mov			edi, [edi]

			mov			ebx, Matrix_Result					// ������� � ���� ������ ������� - ����������
			add			ebx, flagSTR3
			lea			ebx, [ebx]
			mov			ebx, [ebx]

			//������� � ������ ������� ������� ������ �������
			lea			ESI, Matrix2
			mov			esi, [esi]
			mov			esi, [esi]

			add			flagSTR3, 4								// ��� ����������� �� ������� 1-� �������
			mov			flagSTR2, 0								// ���������
			mov			x, 0
			pop			edx										// ���������� ����������� � ���������� ��������
			dec			edx
			cmp			edx, 0
			jne			MET1

			mov			flagSTR, 4
			mov			flagSTR2, 0
			mov			flagSTR3, 4
			mov			x, 0
			mov			flag_M3, 0

			mov			eax, Matrix_Result						// �����
			mov			tmp, eax
			jmp			PRINT




		END:


	}
	std::system("pause");

}

